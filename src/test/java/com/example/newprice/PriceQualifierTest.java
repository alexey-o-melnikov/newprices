package com.example.newprice;

import com.example.newprice.model.Price;
import com.example.newprice.dao.PriceDao;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PriceQualifierTest {

    private static PriceQualifier priceQualifier;

    @BeforeClass
    public static void init() {
        priceQualifier = new PriceQualifier();
        PriceDao priceDao = mock(PriceDao.class);
        when(priceDao.allCurrentPrice()).thenReturn(getCurrenntPrices());
        priceQualifier.setPriceDao(priceDao);
    }

    private static Collection<Price> getCurrenntPrices() {
        Set<Price> prices = new HashSet<>();

        Price price1 = new Price();
        price1.setId(1L);
        price1.setProductCode("122856");
        price1.setNumber(1);
        price1.setDepart(1);
        price1.setBegin(ZonedDateTime.of(2013, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC")));
        price1.setEnd(ZonedDateTime.of(2013, 1, 31, 23, 59, 59, 0, ZoneId.of("UTC")));
        price1.setValue(11000L);
        prices.add(price1);

        Price price2 = new Price();
        price2.setId(2L);
        price2.setProductCode("122856");
        price2.setNumber(2);
        price2.setDepart(1);
        price2.setBegin(ZonedDateTime.of(2013, 1, 10, 0, 0, 0, 0, ZoneId.of("UTC")));
        price2.setEnd(ZonedDateTime.of(2013, 1, 20, 23, 59, 59, 0, ZoneId.of("UTC")));
        price2.setValue(99000L);
        prices.add(price2);

        Price price3 = new Price();
        price3.setId(3L);
        price3.setProductCode("6654");
        price3.setNumber(1);
        price3.setDepart(2);
        price3.setBegin(ZonedDateTime.of(2013, 1, 10, 0, 0, 0, 0, ZoneId.of("UTC")));
        price3.setEnd(ZonedDateTime.of(2013, 1, 20, 23, 59, 59, 0, ZoneId.of("UTC")));
        price3.setValue(5000L);
        prices.add(price3);

        return prices;
    }

    @Test
    public void unitPricesTest() {
        Collection<Price> pricesToAdd = getPricesToAdd();

        Collection<Price> prices = priceQualifier.unionPrice(pricesToAdd);

        Collection<Price> resultPrices = getResultPrices();

        Assert.assertEquals(resultPrices, prices);
    }

    private Set<Price> getPricesToAdd() {
        Set<Price> pricesToAdd = new HashSet<>();

        Price price1 = new Price();
        price1.setId(4L);
        price1.setProductCode("122856");
        price1.setNumber(1);
        price1.setDepart(1);
        price1.setBegin(ZonedDateTime.of(2013, 1, 20, 0, 0, 0, 0, ZoneId.of("UTC")));
        price1.setEnd(ZonedDateTime.of(2013, 2, 20, 23, 59, 59, 0, ZoneId.of("UTC")));
        price1.setValue(11000L);
        pricesToAdd.add(price1);

        Price price2 = new Price();
        price2.setId(5L);
        price2.setProductCode("122856");
        price2.setNumber(2);
        price2.setDepart(1);
        price2.setBegin(ZonedDateTime.of(2013, 1, 15, 0, 0, 0, 0, ZoneId.of("UTC")));
        price2.setEnd(ZonedDateTime.of(2013, 1, 25, 23, 59, 59, 0, ZoneId.of("UTC")));
        price2.setValue(92000L);
        pricesToAdd.add(price2);

        Price price3 = new Price();
        price3.setId(6L);
        price3.setProductCode("6654");
        price3.setNumber(1);
        price3.setDepart(2);
        price3.setBegin(ZonedDateTime.of(2013, 1, 12, 0, 0, 0, 0, ZoneId.of("UTC")));
        price3.setEnd(ZonedDateTime.of(2013, 1, 13, 23, 59, 59, 0, ZoneId.of("UTC")));
        price3.setValue(4000L);
        pricesToAdd.add(price3);

        return pricesToAdd;
    }

    private Set<Price> getResultPrices() {
        Set<Price> pricesToAdd = new HashSet<>();

        Price price1 = new Price();
        price1.setId(10L);
        price1.setProductCode("122856");
        price1.setNumber(1);
        price1.setDepart(1);
        price1.setBegin(ZonedDateTime.of(2013, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC")));
        price1.setEnd(ZonedDateTime.of(2013, 2, 20, 23, 59, 59, 0, ZoneId.of("UTC")));
        price1.setValue(11000L);
        pricesToAdd.add(price1);

        Price price2 = new Price();
        price2.setId(20L);
        price2.setProductCode("122856");
        price2.setNumber(2);
        price2.setDepart(1);
        price2.setBegin(ZonedDateTime.of(2013, 1, 10, 0, 0, 0, 0, ZoneId.of("UTC")));
        price2.setEnd(ZonedDateTime.of(2013, 1, 15, 0, 0, 0, 0, ZoneId.of("UTC")));
        price2.setValue(99000L);
        pricesToAdd.add(price2);

        Price price3 = new Price();
        price3.setId(30L);
        price3.setProductCode("122856");
        price3.setNumber(2);
        price3.setDepart(1);
        price3.setBegin(ZonedDateTime.of(2013, 1, 15, 0, 0, 0, 0, ZoneId.of("UTC")));
        price3.setEnd(ZonedDateTime.of(2013, 1, 25, 23, 59, 59, 0, ZoneId.of("UTC")));
        price3.setValue(4000L);
        pricesToAdd.add(price3);

        Price price4 = new Price();
        price4.setId(40L);
        price4.setProductCode("6654");
        price4.setNumber(1);
        price4.setDepart(2);
        price4.setBegin(ZonedDateTime.of(2013, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC")));
        price4.setEnd(ZonedDateTime.of(2013, 1, 12, 0, 0, 0, 0, ZoneId.of("UTC")));
        price4.setValue(5000L);
        pricesToAdd.add(price4);

        Price price5 = new Price();
        price5.setId(50L);
        price5.setProductCode("6654");
        price5.setNumber(1);
        price5.setDepart(2);
        price5.setBegin(ZonedDateTime.of(2013, 1, 12, 0, 0, 0, 0, ZoneId.of("UTC")));
        price5.setEnd(ZonedDateTime.of(2013, 1, 13, 0, 0, 0, 0, ZoneId.of("UTC")));
        price5.setValue(4000L);
        pricesToAdd.add(price5);

        Price price6 = new Price();
        price6.setId(60L);
        price6.setProductCode("6654");
        price6.setNumber(1);
        price6.setDepart(2);
        price6.setBegin(ZonedDateTime.of(2013, 1, 13, 0, 0, 0, 0, ZoneId.of("UTC")));
        price6.setEnd(ZonedDateTime.of(2013, 1, 31, 0, 0, 0, 0, ZoneId.of("UTC")));
        price6.setValue(5000L);
        pricesToAdd.add(price6);

        return pricesToAdd;
    }
}
