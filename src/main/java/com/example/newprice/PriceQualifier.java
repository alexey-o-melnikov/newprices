package com.example.newprice;

import com.example.newprice.model.Price;
import com.example.newprice.dao.PriceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class PriceQualifier {

    private PriceDao priceDao;

    @Autowired
    public void setPriceDao(PriceDao priceDao) {
        this.priceDao = priceDao;
    }

    public Collection<Price> unionPrice(Collection<Price> pricesToAdd) {
        Collection<Price> prices = priceDao.allCurrentPrice();

        return prices;
    }
}
