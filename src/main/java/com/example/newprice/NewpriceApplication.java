package com.example.newprice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewpriceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewpriceApplication.class, args);
	}
}
