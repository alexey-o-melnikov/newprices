package com.example.newprice.model;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class Price {
    private Long id;             // идентификатор в БД
    private String productCode;  // код товара
    private Integer number;      // номер цены
    private Integer depart;      // номер отдела
    private ZonedDateTime begin; // начало действия
    private ZonedDateTime end;   // конец действия
    private Long value;          // значение цены в копейках
}
