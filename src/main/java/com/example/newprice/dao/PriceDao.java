package com.example.newprice.dao;

import com.example.newprice.model.Price;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PriceDao {
    Collection<Price> allCurrentPrice();
}
